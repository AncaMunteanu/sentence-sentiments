#include "AmazonNegResponse.h"



AmazonNegResponse::AmazonNegResponse() : Response("AmazonN")
{
}

std::string AmazonNegResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("AmazonN").c_str();

	vector <pair<string, string>> amazonPos;
	vector <pair<string, string>> amazonNeg;

	string sentence;
	string type;

	ifstream amazonFile("amazon.txt");
	while (getline(amazonFile, sentence))
	{
		type = sentence.back();
		sentence.pop_back();

		if (type == "1")
			amazonPos.push_back(make_pair(type, sentence));
		else amazonNeg.push_back(make_pair(type, sentence));
	}

	amazonFile.close();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "amazonNegatives.txt"));

	int nr = 1;

	for (auto it : amazonNeg)
	{
		this->content.push_back(boost::property_tree::ptree::value_type(to_string(nr), it.second));
		nr++;
	}

	return this->getContentAsString();
}

