#include "YelpNegResponse.h"



YelpNegResponse::YelpNegResponse() : Response("YelpN")
{
}

std::string YelpNegResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("YelpN").c_str();

	vector <pair<string, string>> yelpPos;
	vector <pair<string, string>> yelpNeg;

	string sentence;
	string type;

	ifstream yelpFile("yelp.txt");
	while (getline(yelpFile, sentence))
	{
		type = sentence.back();
		sentence.pop_back();

		if (type == "1")
			yelpPos.push_back(make_pair(type, sentence));
		else yelpNeg.push_back(make_pair(type, sentence));
	}

	yelpFile.close();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "yelpNegatives.txt"));

	int nr = 1;

	for (auto it : yelpNeg)
	{
		this->content.push_back(boost::property_tree::ptree::value_type(to_string(nr), it.second));
		nr++;
	}

	return this->getContentAsString();
}
