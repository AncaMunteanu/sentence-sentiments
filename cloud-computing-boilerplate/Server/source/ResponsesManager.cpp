#include "ResponsesManager.h"

#include "HandshakeResponse.h"
#include "SumResponse.h"
#include "WordCountResponse.h"
#include "AmazonPosResponse.h"
#include "AmazonNegResponse.h"
#include "ImdbPosResponse.h"
#include "ImdbNegResponse.h"
#include "YelpPosResponse.h"
#include "YelpNegResponse.h"



ResponsesManager::ResponsesManager()
{
	this->Responses.emplace("Handshake", std::make_shared<HandshakeResponse>());
	this->Responses.emplace("Sum", std::make_shared<SumResponse>());
	this->Responses.emplace("WordCounter", std::make_shared<WordCountResponse>());
	this->Responses.emplace("AmazonP", std::make_shared<AmazonPosResponse>());
	this->Responses.emplace("AmazonN", std::make_shared<AmazonNegResponse>());
	this->Responses.emplace("ImdbP", std::make_shared<ImdbPosResponse>());
	this->Responses.emplace("ImdbN", std::make_shared<ImdbNegResponse>());
	this->Responses.emplace("YelpP", std::make_shared<YelpPosResponse>());
	this->Responses.emplace("YelpN", std::make_shared<YelpNegResponse>());


}

std::map<std::string, std::shared_ptr<Framework::Response>> ResponsesManager::getMap() const
{
	return this->Responses;
}
