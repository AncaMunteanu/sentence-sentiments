#include "AmazonTopTenReponse.h"



AmazonTopTenReponse::AmazonTopTenReponse() : Response("AmazonTopTen")
{
}

void AmazonTopTenReponse::read()
{
	std::string sentence;

	std::ifstream amazonFile("amazon.txt");
	while (getline(amazonFile, sentence))
	{
		sentence.pop_back();
		amazonTT.push_back(sentence);
	}
	amazonFile.close();
}

bool comparator(std::string i, std::string j)
{
	return (i.length() < j.length());
}

std::string AmazonTopTenReponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("AmazonTT");

	read();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "amazonTopTen.txt"));

	std::sort(amazonTT.begin(), amazonTT.end(), comparator);
	for (int i = amazonTT.size() - 10; i < amazonTT.size(); i++)
		std::cout << i << ' ' << amazonTT[i] << std::endl;

	amazonTT.clear();
	amazonTT.shrink_to_fit();

	return this->getContentAsString();

}
