#include "YelpNegConoResponse.h"



YelpNegConoResponse::YelpNegConoResponse() : Response("YelpNegativeConotations")
{
}

void YelpNegConoResponse::read()
{
	std::string sentence;
	std::string type;

	std::ifstream yelpFile("yelp.txt");
	while (getline(yelpFile, sentence))
	{
		type = sentence.back();
		sentence.pop_back();

		if (type == "0")
			yelpNeg.push_back(make_pair(type, sentence));
	}
	yelpFile.close();
}

void YelpNegConoResponse::addWord(std::string word)
{
	bool found = false;
	for (int i = 0; i < words.size(); i++)
		if (word == words[i])
		{
			frequency[i]++;
			found = true;
			break;
		}

	if (found == false)
	{
		words.push_back(word);
		frequency.push_back(1);
	}
}

void YelpNegConoResponse::splitSentences(std::string str)
{
	std::string word;

	std::stringstream iss(str);

	while (iss >> word)
		if (word.size() >= 5)
			addWord(word);
}

std::string YelpNegConoResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("YelpNC").c_str();

	read();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "yelpNegativeConotations.txt"));

	for (auto it : yelpNeg)
		splitSentences(it.second);

	int max1 = 0, max2 = 0, max3 = 0, max4 = 0, max5 = 0;
	std::string sent1, sent2, sent3, sent4, sent5;
	for (int i = 0; i < frequency.size(); i++)
		if (frequency[i] > max1)
		{
			max5 = max4;
			sent5 = sent4;

			max4 = max3;
			sent4 = sent3;

			max3 = max2;
			sent3 = sent2;

			max2 = max1;
			sent2 = sent1;

			max1 = frequency[i];
			sent1 = words[i];
		}
		else if (frequency[i] > max2)
		{
			max5 = max4;
			sent5 = sent4;

			max4 = max3;
			sent4 = sent3;

			max3 = max2;
			sent3 = sent2;

			max2 = frequency[i];
			sent2 = words[i];
		}
		else if (frequency[i] > max3)
		{
			max5 = max4;
			sent5 = sent4;

			max4 = max3;
			sent4 = sent3;

			max3 = frequency[i];
			sent3 = words[i];
		}
		else if (frequency[i] > max4)
		{
			max5 = max4;
			sent5 = sent4;

			max4 = frequency[i];
			sent4 = words[i];
		}
		else if (frequency[i] > max5)
		{
			max5 = frequency[i];
			sent5 = words[i];
		}

	this->content.push_back(boost::property_tree::ptree::value_type(std::to_string(max1), sent1));
	this->content.push_back(boost::property_tree::ptree::value_type(std::to_string(max2), sent2));
	this->content.push_back(boost::property_tree::ptree::value_type(std::to_string(max3), sent3));
	this->content.push_back(boost::property_tree::ptree::value_type(std::to_string(max4), sent4));
	this->content.push_back(boost::property_tree::ptree::value_type(std::to_string(max5), sent5));

	yelpNeg.clear();
	yelpNeg.shrink_to_fit();
	words.clear();
	words.shrink_to_fit();
	frequency.clear();
	frequency.shrink_to_fit();

	return this->getContentAsString();
}