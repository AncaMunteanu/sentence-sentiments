#include "ImdbPosResponse.h"



ImdbPosResponse::ImdbPosResponse() : Response("ImdbP")
{
}

std::string ImdbPosResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("ImdbP").c_str();

	vector <pair<string, string>> imdbPos;
	vector <pair<string, string>> imdbNeg;

	string sentence;
	string type;

	ifstream imdbFile("imdb.txt");
	while (getline(imdbFile, sentence))
	{
		type = sentence.back();
		sentence.pop_back();

		if (type == "1")
			imdbPos.push_back(make_pair(type, sentence));
		else imdbNeg.push_back(make_pair(type, sentence));
	}

	imdbFile.close();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "imdbPositives.txt"));

	int nr = 1;

	for (auto it : imdbPos)
	{
		this->content.push_back(boost::property_tree::ptree::value_type(to_string(nr), it.second));
		nr++;
	}

	return this->getContentAsString();
}
