#include "ImdbWordResponse.h"



ImdbWordResponse::ImdbWordResponse() : Response("ImdbWord")
{
}

void ImdbWordResponse::read()
{
	std::string sentence;

	std::ifstream imdbFile("imdb.txt");
	while (getline(imdbFile, sentence))
	{
		sentence.pop_back();
		imdbReviews.push_back(sentence);
	}
	imdbFile.close();
}

std::string ImdbWordResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto word = packet.get<std::string>("ImdbW");

	read();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "imdbReviewsWithWord.txt"));

	std::size_t found;
	int nr = 1;
	for (std::string it : imdbReviews)
	{
		found = it.find(word);
		if (found != std::string::npos)
		{
			this->content.push_back(boost::property_tree::ptree::value_type(std::to_string(nr), it));
			nr++;
		}
	}

	imdbReviews.clear();
	imdbReviews.shrink_to_fit();

	return this->getContentAsString();
}
