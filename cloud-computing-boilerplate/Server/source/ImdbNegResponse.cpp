#include "ImdbNegResponse.h"



ImdbNegResponse::ImdbNegResponse() : Response("ImdbN")
{
}

std::string ImdbNegResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("ImdbN").c_str();

	vector <pair<string, string>> imdbPos;
	vector <pair<string, string>> imdbNeg;

	string sentence;
	string type;

	ifstream imdbFile("imdb.txt");
	while (getline(imdbFile, sentence))
	{
		type = sentence.back();
		sentence.pop_back();

		if (type == "1")
			imdbPos.push_back(make_pair(type, sentence));
		else imdbNeg.push_back(make_pair(type, sentence));
	}

	imdbFile.close();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "imdbNegatives.txt"));

	int nr = 1;

	for (auto it : imdbNeg)
	{
		this->content.push_back(boost::property_tree::ptree::value_type(to_string(nr), it.second));
		nr++;
	}

	return this->getContentAsString();
}
