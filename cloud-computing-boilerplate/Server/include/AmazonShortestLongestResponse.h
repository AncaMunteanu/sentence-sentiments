#pragma once
#include "Response.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
class AmazonShortestLongestResponse : public Framework::Response
{
public:
	AmazonShortestLongestResponse();
	void read();
	std::string interpretPacket(const boost::property_tree::ptree& packet);

	std::vector <std::string> amazonSL;
};

