#pragma once
#include "Response.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;
class YelpNegResponse : public Framework::Response
{
public:
	YelpNegResponse();
	std::string interpretPacket(const boost::property_tree::ptree& packet);

};

