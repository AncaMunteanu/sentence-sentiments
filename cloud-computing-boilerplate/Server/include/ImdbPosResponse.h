#pragma once
#include "Response.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;
class ImdbPosResponse : public Framework::Response
{
public:
	ImdbPosResponse();
	std::string interpretPacket(const boost::property_tree::ptree& packet);

};

