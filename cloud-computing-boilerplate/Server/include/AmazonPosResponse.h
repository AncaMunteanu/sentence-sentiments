#pragma once
#include "Response.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

class AmazonPosResponse : public Framework::Response
{
public:
	AmazonPosResponse();
	std::string interpretPacket(const boost::property_tree::ptree& packet);
};

