#pragma once
#include "Response.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

class ImdbWordResponse : public Framework::Response
{
public:
	ImdbWordResponse();
	void read();
	std::string interpretPacket(const boost::property_tree::ptree& packet);

	std::vector <std::string> imdbReviews;
};

