#pragma once

#include "Response.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

class ImdbTopTenResponse : public Framework::Response
{
public:
	ImdbTopTenResponse();
	void read();
	friend bool comparator(std::string, std::string);
	std::string interpretPacket(const boost::property_tree::ptree& packet);

	std::vector < std::string> imdbTT;
};

