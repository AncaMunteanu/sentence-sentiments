#include "ImdbTopTenRequest.h"



ImdbTopTenRequest::ImdbTopTenRequest() : Request("ImdbTopTen")
{
	this->content.push_back(boost::property_tree::ptree::value_type("ImdbTT", "Top ten most detailed reviews, please!"));
}
