#include <iostream>
#include <memory>

#include "Session.h"

int main(int argc, char** argv)
{
	const auto host = "127.0.0.1";
	const auto port = "2751";

	// The io_context is required for all I/O
	boost::asio::io_context ioContext;

	RequestsManager requestsManager;

	std::string handshake, sum, wordcounter, amazonPos, amazonNeg, imdbPos, imdbNeg, yelpPos, yelpNeg;

	int choice = 0;
	bool solveReq = true;

	//std::cout << "Choose procedure: "; //unfinished
	//std::cin >> choice;

	/*while (solveReq)
	//{
		std::cin >> choice;
	//	switch (choice)
	//	{
		case 1:*/
			handshake = requestsManager.getMap().at("Handshake")->getContentAsString();
			std::make_shared<Session>(ioContext)->run(host, port, handshake);

	/*		break;
		case 2:
			sum = requestsManager.getMap().at("Sum")->getContentAsString();
			break;
		case 3:
			wordcounter = requestsManager.getMap().at("WordCounter")->getContentAsString();
			break;
		case 4:
			amazonPos = requestsManager.getMap().at("AmazonP")->getContentAsString();
			break;
		case 5:
			amazonNeg = requestsManager.getMap().at("AmazonN")->getContentAsString();
			break;
		case 6:*/
//			imdbPos = requestsManager.getMap().at("ImdbP")->getContentAsString();
	//		break;
	//	case 7:
//			imdbNeg = requestsManager.getMap().at("ImdbN")->getContentAsString();
	//		break;
	//	case 8:
//			yelpPos = requestsManager.getMap().at("YelpP")->getContentAsString();
	//		break;
	//	case 9:
			//yelpNeg = requestsManager.getMap().at("YelpN")->getContentAsString();
	/*		break;
		default: choice = 0;
		}
	} //while (choice != 0);*/
	
	

	// Launch the asynchronous operation
	//std::make_shared<Session>(ioContext)->run(host, port, handshake);
	/*std::make_shared<Session>(ioContext)->run(host, port, sum);
	std::make_shared<Session>(ioContext)->run(host, port, wordcounter);
	std::make_shared<Session>(ioContext)->run(host, port, amazonPos);
	std::make_shared<Session>(ioContext)->run(host, port, amazonNeg);*/
//	std::make_shared<Session>(ioContext)->run(host, port, imdbPos);
//	std::make_shared<Session>(ioContext)->run(host, port, imdbNeg);
//	std::make_shared<Session>(ioContext)->run(host, port, yelpPos);
	//std::make_shared<Session>(ioContext)->run(host, port, yelpNeg);


	// Run the I/O service. The call will return when
	// the socket is closed.
	ioContext.run();
	

	return EXIT_SUCCESS;
}