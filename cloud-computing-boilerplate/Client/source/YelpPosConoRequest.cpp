#include "YelpPosConoRequest.h"



YelpPosConoRequest::YelpPosConoRequest() : Request("YelpPositiveConotations")
{
	this->content.push_back(boost::property_tree::ptree::value_type("YelpPC", "Top 5 words with positive conotations, please!"));
}
