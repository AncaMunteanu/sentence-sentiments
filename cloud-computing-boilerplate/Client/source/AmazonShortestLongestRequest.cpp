#include "AmazonShortestLongestRequest.h"



AmazonShortestLongestRequest::AmazonShortestLongestRequest() : Request("AmazonShortestLongest")
{
	this->content.push_back(boost::property_tree::ptree::value_type("AmazonSL", "Shortest and longest review, please!"));

}

