#include "AmazonTopTenRequest.h"



AmazonTopTenRequest::AmazonTopTenRequest() : Request("AmazonTopTen")
{
	this->content.push_back(boost::property_tree::ptree::value_type("AmazonTT", "Top ten most detailed reviews, please!"));
}