#include "RequestsManager.h"

#include "HandshakeRequest.h"
#include "SumRequest.h"
#include "WordCountRequest.h"
#include "AmazonPosRequest.h"
#include "AmazonNegRequest.h"
#include "ImdbPosRequest.h"
#include "ImdbNegRequest.h"
#include "YelpPosRequest.h"
#include "YelpNegRequest.h"


RequestsManager::RequestsManager()
{
	this->requests.emplace("Handshake", std::make_shared<HandshakeRequest>());
	this->requests.emplace("Sum", std::make_shared<SumRequest>());
	this->requests.emplace("WordCounter", std::make_shared<WordCountRequest>());
	this->requests.emplace("AmazonP", std::make_shared<AmazonPosRequest>());
	this->requests.emplace("AmazonN", std::make_shared<AmazonNegRequest>());
	this->requests.emplace("ImdbP", std::make_shared<ImdbPosRequest>());
	this->requests.emplace("ImdbN", std::make_shared<ImdbNegRequest>());
	this->requests.emplace("YelpP", std::make_shared<YelpPosRequest>());
	this->requests.emplace("YelpN", std::make_shared<YelpNegRequest>());

}

std::map<std::string, std::shared_ptr<Framework::Request>> RequestsManager::getMap() const
{
	return this->requests;
}
