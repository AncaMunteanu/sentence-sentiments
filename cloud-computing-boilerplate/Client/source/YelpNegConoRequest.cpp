#include "YelpNegConoRequest.h"



YelpNegConoRequest::YelpNegConoRequest() : Request("YelpNegativeConotations")
{
	this->content.push_back(boost::property_tree::ptree::value_type("YelpNC", "Top 5 words with negative conotations, please!"));
}
