#pragma once
#include "Response.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>

class YelpNegConoResponse : public Framework::Response
{
public:
	YelpNegConoResponse();
	void read();
	void addWord(std::string);
	void splitSentences(std::string);
	std::string interpretPacket(const boost::property_tree::ptree& packet);


	std::vector<std::pair<std::string, std::string>> yelpNeg;
	std::vector<std::string> words;
	std::vector<int> frequency;
};

