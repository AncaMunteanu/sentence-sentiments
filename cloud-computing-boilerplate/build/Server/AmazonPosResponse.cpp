#include "AmazonPosResponse.h"
#include <stdlib.h>


AmazonPosResponse::AmazonPosResponse() : Response("AmazonP")
{
}

void AmazonPosResponse::read()
{
	string sentence;
	string type;

	ifstream amazonFile("amazon.txt");
	while (getline(amazonFile, sentence))
	{
		type = sentence.back();
		sentence.pop_back();

		if (type == "1")
			amazonPos.push_back(make_pair(type, sentence));
	}
	amazonFile.close();
}

std::string AmazonPosResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("AmazonP").c_str();

	read();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "amazonPositives.txt"));
	
	int nr = 1;

	for (auto it : amazonPos)
	{
		this->content.push_back(boost::property_tree::ptree::value_type(to_string(nr), it.second));
		nr++;
	}

	return this->getContentAsString();
}
