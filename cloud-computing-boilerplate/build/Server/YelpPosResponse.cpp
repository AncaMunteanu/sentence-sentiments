#include "YelpPosResponse.h"



YelpPosResponse::YelpPosResponse() : Response("YelpP")
{
}

void YelpPosResponse::read()
{
	string sentence;
	string type;

	ifstream yelpFile("yelp.txt");
	while (getline(yelpFile, sentence))
	{
		type = sentence.back();
		sentence.pop_back();

		if (type == "1")
			yelpPos.push_back(make_pair(type, sentence));
	}
	yelpFile.close();
}

std::string YelpPosResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("YelpP").c_str();

	read();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "yelpPositives.txt"));

	int nr = 1;

	for (auto it : yelpPos)
	{
		this->content.push_back(boost::property_tree::ptree::value_type(to_string(nr), it.second));
		nr++;
	}

	return this->getContentAsString();
}
