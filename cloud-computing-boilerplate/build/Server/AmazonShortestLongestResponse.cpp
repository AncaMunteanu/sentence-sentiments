#include "AmazonShortestLongestResponse.h"



AmazonShortestLongestResponse::AmazonShortestLongestResponse() : Response("AmazonShortestLongest")
{
}

void AmazonShortestLongestResponse::read()
{
	std::string sentence;

	std::ifstream amazonFile("amazon.txt");
	while (getline(amazonFile, sentence))
	{
		sentence.pop_back();
		amazonSL.push_back(sentence);
	}
	amazonFile.close();
}

std::string AmazonShortestLongestResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("AmazonSL");

	read();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "amazonShortLong.txt"));

	std::string shortest = amazonSL[0], longest = amazonSL[0];

	for (auto it : amazonSL)
		if (it.length() < shortest.length())
			shortest = it;
		else if (it.length() >= longest.length())
			longest = it;
	this->content.push_back(boost::property_tree::ptree::value_type("Shortest", shortest));
	this->content.push_back(boost::property_tree::ptree::value_type("Longest", longest));

	amazonSL.clear();
	amazonSL.shrink_to_fit();

	return this->getContentAsString();
}
