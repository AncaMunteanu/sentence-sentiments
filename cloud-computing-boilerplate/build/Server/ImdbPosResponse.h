#pragma once
#include "Response.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

class ImdbPosResponse : public Framework::Response
{
public:
	ImdbPosResponse();
	void read();
	std::string interpretPacket(const boost::property_tree::ptree& packet);

	std::vector <std::pair<std::string, std::string>> imdbPos;
};

