#include "YelpShortestLongestResponse.h"



YelpShortestLongestResponse::YelpShortestLongestResponse() : Response("YelpShortestLongest")
{
}

void YelpShortestLongestResponse::read()
{
	std::string sentence;

	std::ifstream yelpFile("yelp.txt");
	while (getline(yelpFile, sentence))
	{
		sentence.pop_back();
		yelpSL.push_back(sentence);
	}
	yelpFile.close();
}

std::string YelpShortestLongestResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("YelpSL");

	read();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "yelpShortLong.txt"));

	std::string shortest = yelpSL[0], longest = yelpSL[0];

	for (auto it : yelpSL)
		if (it.length() < shortest.length())
			shortest = it;
		else if (it.length() >= longest.length())
			longest = it;
	this->content.push_back(boost::property_tree::ptree::value_type("Shortest", shortest));
	this->content.push_back(boost::property_tree::ptree::value_type("Longest", longest));

	yelpSL.clear();
	yelpSL.shrink_to_fit();

	return this->getContentAsString();
}
