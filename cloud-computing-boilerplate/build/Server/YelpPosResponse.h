#pragma once
#include "Response.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;
class YelpPosResponse : public Framework::Response
{
public:
	YelpPosResponse();
	void read();
	std::string interpretPacket(const boost::property_tree::ptree& packet);

	vector <pair<string, string>> yelpPos;
};

