#include "ImdbPosResponse.h"



ImdbPosResponse::ImdbPosResponse() : Response("ImdbPositive")
{
}

void ImdbPosResponse::read()
{
	std::string sentence;
	std::string type;

	std::ifstream imdbFile("imdb.txt");
	while (getline(imdbFile, sentence))
	{
		type = sentence.back();
		sentence.pop_back();

		if (type == "1")
			imdbPos.push_back(make_pair(type, sentence));
	}
	imdbFile.close();
}

std::string ImdbPosResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("ImdbP").c_str();

	read();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "imdbPositives.txt"));

	int nr = 1;

	for (auto it : imdbPos)
	{
		this->content.push_back(boost::property_tree::ptree::value_type(std::to_string(nr), it.second));
		nr++;
	}

	imdbPos.clear();
	imdbPos.shrink_to_fit();

	return this->getContentAsString();
}
