#include "AmazonFreqResponse.h"
#include <boost/algorithm/string.hpp>


AmazonFreqResponse::AmazonFreqResponse() : Response("AmazonFrequency")
{
}

void AmazonFreqResponse::read()
{
	string sentence;

	ifstream amazonFile("amazon.txt");
	while (getline(amazonFile, sentence))
	{
		sentence.pop_back();
		amazonByAlphabet.push_back(sentence);
	}
	amazonFile.close();
}

std::string AmazonFreqResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("AmazonFreq").c_str();

	read();

	std::sort(amazonByAlphabet.begin(), amazonByAlphabet.end());

	std::vector<std::string>::iterator it = amazonByAlphabet.begin();
	std::vector<std::string>::iterator it1 = it;

	this->content.push_back(boost::property_tree::ptree::value_type("File", "amazonFrequency.txt"));

	int counter;
	while (it < amazonByAlphabet.end() - 1 && it1 < amazonByAlphabet.end() - 1)
	{
		counter = 1;
		it1++;
		if (boost::iequals(*it1, *it))
			while (boost::iequals(*it1, *it))
			{
				it1++;
				counter++;
			}

		this->content.push_back(boost::property_tree::ptree::value_type(to_string(counter), *it));

		if (counter == 1)
			it++;
		else
			it = it1;
	}

	return this->getContentAsString();
}

