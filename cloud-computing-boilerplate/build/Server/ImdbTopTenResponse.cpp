#include "ImdbTopTenResponse.h"



ImdbTopTenResponse::ImdbTopTenResponse() : Response("ImdbTopTen")
{
}

void ImdbTopTenResponse::read()
{
	std::string sentence;

	std::ifstream imdbFile("imdb.txt");
	while (getline(imdbFile, sentence))
	{
		sentence.pop_back();
		imdbTT.push_back(sentence);
	}
	imdbFile.close();
}
/*
bool comparator(std::string i, std::string j)
{
	return (i.length() < j.length());
}
*/
std::string ImdbTopTenResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("ImdbTT");

	read();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "imdbTopTen.txt"));

	std::sort(imdbTT.begin(), imdbTT.end(), comparator);
	for (int i = imdbTT.size() - 10; i < imdbTT.size(); i++)
		this->content.push_back(boost::property_tree::ptree::value_type(std::to_string(i), imdbTT[i]));

	imdbTT.clear();
	imdbTT.shrink_to_fit();

	return this->getContentAsString();
}
