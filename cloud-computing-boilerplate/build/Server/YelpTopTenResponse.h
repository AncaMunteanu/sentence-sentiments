#pragma once
#include "Response.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
class YelpTopTenResponse : public Framework::Response
{
public:
	YelpTopTenResponse();
	void read();
	friend bool comparator(std::string, std::string);
	std::string interpretPacket(const boost::property_tree::ptree& packet);

	std::vector < std::string> yelpTT;
};

