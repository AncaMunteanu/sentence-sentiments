#include "YelpWordResponse.h"



YelpWordResponse::YelpWordResponse() : Response("YelpWord")
{
}

void YelpWordResponse::read()
{
	std::string sentence;

	std::ifstream yelpFile("yelp.txt");
	while (getline(yelpFile, sentence))
	{
		sentence.pop_back();
		yelpReviews.push_back(sentence);
	}
	yelpFile.close();
}

std::string YelpWordResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto word = packet.get<std::string>("YelpW");

	read();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "yelpReviewsWithWord.txt"));

	std::size_t found;
	int nr = 1;
	for (std::string it : yelpReviews)
	{
		found = it.find(word);
		if (found != std::string::npos)
		{
			this->content.push_back(boost::property_tree::ptree::value_type(std::to_string(nr), it));
			nr++;
		}
	}

	yelpReviews.clear();
	yelpReviews.shrink_to_fit();

	return this->getContentAsString();
}
