#include "ImdbShortestLongestResponse.h"



ImdbShortestLongestResponse::ImdbShortestLongestResponse() : Response("ImdbShortestLongest")
{
}

void ImdbShortestLongestResponse::read()
{
	std::string sentence;

	std::ifstream imdbFile("imdb.txt");
	while (getline(imdbFile, sentence))
	{
		sentence.pop_back();
		imdbSL.push_back(sentence);
	}
	imdbFile.close();
}

std::string ImdbShortestLongestResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("ImdbSL");

	read();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "imdbShortLong.txt"));

	std::string shortest = imdbSL[0], longest = imdbSL[0];

	for (auto it : imdbSL)
		if (it.length() < shortest.length())
			shortest = it;
		else if (it.length() >= longest.length())
			longest = it;
	this->content.push_back(boost::property_tree::ptree::value_type("Shortest", shortest));
	this->content.push_back(boost::property_tree::ptree::value_type("Longest", longest));

	imdbSL.clear();
	imdbSL.shrink_to_fit();

	return this->getContentAsString();
}
