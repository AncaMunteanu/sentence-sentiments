#include "YelpFreqResponse.h"
#include <boost/algorithm/string.hpp>


YelpFreqResponse::YelpFreqResponse() : Response("YelpFrequency")
{
}

void YelpFreqResponse::read()
{
	string sentence;

	ifstream yelpFile("yelp.txt");
	while (getline(yelpFile, sentence))
	{
		sentence.pop_back();
		yelpByAlphabet.push_back(sentence);
	}
	yelpFile.close();
}

std::string YelpFreqResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("YelpFreq").c_str();

	read();

	std::sort(yelpByAlphabet.begin(), yelpByAlphabet.end());

	std::vector<std::string>::iterator it = yelpByAlphabet.begin();
	std::vector<std::string>::iterator it1 = it;

	this->content.push_back(boost::property_tree::ptree::value_type("File", "yelpFrequency.txt"));

	int counter;
	while (it < yelpByAlphabet.end() - 1 && it1 < yelpByAlphabet.end() - 1)
	{
		counter = 1;
		it1++;
		if (boost::iequals(*it1, *it))
			while (boost::iequals(*it1, *it))
			{
				it1++;
				counter++;
			}

		this->content.push_back(boost::property_tree::ptree::value_type(to_string(counter), *it));

		if (counter == 1)
			it++;
		else
			it = it1;
	}
	return this->getContentAsString();
}
