#pragma once
#include "Response.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

class YelpWordResponse : public Framework::Response
{
public:
	YelpWordResponse();
	void read();
	std::string interpretPacket(const boost::property_tree::ptree& packet);

	std::vector <std::string> yelpReviews;
};

