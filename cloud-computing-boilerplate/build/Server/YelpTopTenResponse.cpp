#include "YelpTopTenResponse.h"



YelpTopTenResponse::YelpTopTenResponse() : Response("YelpTopTen")
{
}

void YelpTopTenResponse::read()
{
	std::string sentence;

	std::ifstream yelpFile("yelp.txt");
	while (getline(yelpFile, sentence))
	{
		sentence.pop_back();
		yelpTT.push_back(sentence);
	}
	yelpFile.close();
}

std::string YelpTopTenResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("YelpTT");

	read();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "yelpTopTen.txt"));

	std::sort(yelpTT.begin(), yelpTT.end(), comparator);
	for (int i = yelpTT.size() - 10; i < yelpTT.size(); i++)
		this->content.push_back(boost::property_tree::ptree::value_type(std::to_string(i), yelpTT[i]));

	yelpTT.clear();
	yelpTT.shrink_to_fit();

	return this->getContentAsString();
}
