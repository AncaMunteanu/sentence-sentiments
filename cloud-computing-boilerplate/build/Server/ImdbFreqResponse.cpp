#include "ImdbFreqResponse.h"
#include <boost/algorithm/string.hpp>


ImdbFreqResponse::ImdbFreqResponse() : Response("ImdbFrequency")
{
}

void ImdbFreqResponse::read()
{
	string sentence;

	ifstream imdbFile("imdb.txt");
	while (getline(imdbFile, sentence))
	{
		sentence.pop_back();
		imdbByAlphabet.push_back(sentence);
	}
	imdbFile.close();
}

std::string ImdbFreqResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("ImdbFreq").c_str();

	read();

	std::sort(imdbByAlphabet.begin(), imdbByAlphabet.end());

	std::vector<std::string>::iterator it = imdbByAlphabet.begin();
	std::vector<std::string>::iterator it1 = it;

	this->content.push_back(boost::property_tree::ptree::value_type("File", "imdbFrequency.txt"));

	int counter;
	while (it < imdbByAlphabet.end() - 1 && it1 < imdbByAlphabet.end() - 1)
	{
		counter = 1;
		it1++;
		if (boost::iequals(*it1, *it))
			while (boost::iequals(*it1, *it))
			{
				it1++;
				counter++;
			}

		this->content.push_back(boost::property_tree::ptree::value_type(to_string(counter), *it));

		if (counter == 1)
			it++;
		else
			it = it1;
	}
	return this->getContentAsString();
}
