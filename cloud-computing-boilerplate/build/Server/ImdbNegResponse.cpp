#include "ImdbNegResponse.h"



ImdbNegResponse::ImdbNegResponse() : Response("ImdbN")
{
}

void ImdbNegResponse::read()
{
	string sentence;
	string type;

	ifstream imdbFile("imdb.txt");
	while (getline(imdbFile, sentence))
	{
		type = sentence.back();
		sentence.pop_back();

		if (type == "0")
			 imdbNeg.push_back(make_pair(type, sentence));
	}
	imdbFile.close();
}

std::string ImdbNegResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("ImdbN").c_str();

	read();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "imdbNegatives.txt"));

	int nr = 1;

	for (auto it : imdbNeg)
	{
		this->content.push_back(boost::property_tree::ptree::value_type(to_string(nr), it.second));
		nr++;
	}

	return this->getContentAsString();
}
