#pragma once
#include "Response.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class AmazonFreqResponse : public Framework::Response
{
public:
	AmazonFreqResponse();
	void read();
	std::string interpretPacket(const boost::property_tree::ptree& packet);

	vector <string> amazonByAlphabet;
};
