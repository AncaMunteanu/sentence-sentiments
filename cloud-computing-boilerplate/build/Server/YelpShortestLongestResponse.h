#pragma once
#include "Response.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
class YelpShortestLongestResponse : public Framework::Response
{
public:
	YelpShortestLongestResponse();
	void read();
	std::string interpretPacket(const boost::property_tree::ptree& packet);

	std::vector <std::string> yelpSL;
};

