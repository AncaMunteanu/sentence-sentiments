#pragma once
#include "Response.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
class ImdbShortestLongestResponse : public Framework::Response
{
public:
	ImdbShortestLongestResponse();
	void read();
	std::string interpretPacket(const boost::property_tree::ptree& packet);

	std::vector <std::string> imdbSL;
};

