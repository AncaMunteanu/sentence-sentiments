#pragma once
#include "Response.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;
class ImdbNegResponse : public Framework::Response
{
public:
	ImdbNegResponse();
	void read();
	std::string interpretPacket(const boost::property_tree::ptree& packet);

	vector <pair<string, string>> imdbNeg;
};

