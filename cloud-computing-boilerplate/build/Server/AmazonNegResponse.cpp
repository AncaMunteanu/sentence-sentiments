#include "AmazonNegResponse.h"



AmazonNegResponse::AmazonNegResponse() : Response("AmazonN")
{
}

void AmazonNegResponse::read()
{
	string sentence;
	string type;

	ifstream amazonFile("amazon.txt");
	while (getline(amazonFile, sentence))
	{
		type = sentence.back();
		sentence.pop_back();

		if (type == "0")
			amazonNeg.push_back(make_pair(type, sentence));
	}
	amazonFile.close();
}

std::string AmazonNegResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	auto clientRequest = packet.get<std::string>("AmazonN").c_str();

	read();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "amazonNegatives.txt"));

	int nr = 1;

	for (auto it : amazonNeg)
	{
		this->content.push_back(boost::property_tree::ptree::value_type(to_string(nr), it.second));
		nr++;
	}

	return this->getContentAsString();
}//fara using namespace std

