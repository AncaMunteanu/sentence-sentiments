#include "ImdbShortestLongestRequest.h"



ImdbShortestLongestRequest::ImdbShortestLongestRequest() : Request("YelpShortestLongest")
{
	this->content.push_back(boost::property_tree::ptree::value_type("YelpSL", "Shortest and longest review, please!"));
}
