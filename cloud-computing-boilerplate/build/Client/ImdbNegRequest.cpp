#include "ImdbNegRequest.h"



ImdbNegRequest::ImdbNegRequest() : Request("ImdbN")
{
	this->content.push_back(boost::property_tree::ptree::value_type("ImdbN", "Negative reviews, please!"));

}
