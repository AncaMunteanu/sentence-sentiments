#include "ImdbFreqRequest.h"

ImdbFreqRequest::ImdbFreqRequest() : Request("ImdbFrequency")
{
	this->content.push_back(boost::property_tree::ptree::value_type("ImdbFreq", "Frequency of reviews, please!"));
}