#include "ImdbNegConoRequest.h"



ImdbNegConoRequest::ImdbNegConoRequest() : Request("ImdbNegativeConotations")
{
	this->content.push_back(boost::property_tree::ptree::value_type("ImdbNC", "Top 5 words with negative conotations, please!"));
}
