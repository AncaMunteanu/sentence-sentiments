#include "YelpNegRequest.h"



YelpNegRequest::YelpNegRequest() : Request("YelpN")
{
	this->content.push_back(boost::property_tree::ptree::value_type("YelpN", "Negative reviews, please!"));
}

