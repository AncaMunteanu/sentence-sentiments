#include "YelpShortestLongestRequest.h"



YelpShortestLongestRequest::YelpShortestLongestRequest() : Request("ImdbShortestLongest")
{
	this->content.push_back(boost::property_tree::ptree::value_type("ImdbSL", "Shortest and longest review, please!"));
}

