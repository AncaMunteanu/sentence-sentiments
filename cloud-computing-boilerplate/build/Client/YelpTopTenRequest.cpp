#include "YelpTopTenRequest.h"



YelpTopTenRequest::YelpTopTenRequest() : Request("YelpTopTen")
{
	this->content.push_back(boost::property_tree::ptree::value_type("YelpTT", "Top ten most detailed reviews, please!"));
}
