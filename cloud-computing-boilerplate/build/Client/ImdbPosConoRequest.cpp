#include "ImdbPosConoRequest.h"



ImdbPosConoRequest::ImdbPosConoRequest() : Request("ImdbPositiveConotations")
{
	this->content.push_back(boost::property_tree::ptree::value_type("ImdbPC", "Top 5 words with positive conotations, please!"));
}
